# mesos-setup

Mesos and marathon configuration files and misc - using mesosphere template for Amazon Cloudformation.

## what should your cluster look like

There are 2 groups in mesos: the masters and the slaves.
The number of masters determines the redundancy (HA).
The number of slaves determines the horse power.
There are 2 subgroups of slaves:

* public slaves: for services that should be accessible 'from the outside', like web interfaces.  The outside can be limited to a VPN still, but it's for open access of a certain port (within the VPN) - like polling elasticsearch.
* non-public slaves: can only be accessed from the master.

One can determine the number of slaves, masters in the templates.

TODO: There is a third type of machine in the cluster which still needs to be investigated - I suspect it might have a communication function, but need to see the software installed on it.


## Prerequisites for managing

Installing the dcos client (requires python and virtualenv)

    mkdir -p dcos && cd dcos && \
      curl -O https://downloads.mesosphere.io/dcos-cli/install.sh && \
          bash ./install.sh . http://$master_box && \
                source ./bin/env-setup

# Various useful links:

Get master address from AWS cloudformation console, click the stack and look at Output

(note the master.mesos, which is valid if you add your dns servers correctly like [so](#dns))

admin console

    http://master.mesos

Logs

    http://master.mesos/mesos

Zookeeper

    http://master.mesos/exhibitor

To ssh onto cluster (ip to be found on the left hand side of the admin console):

    ssh -i $key_location core@$$master_ip

The marathon REST API on port 8080 of the master ip: https://mesosphere.github.io/marathon/docs/rest-api.html 
For instance, find which endpoints tasks are running on:

    http://master.mesos:8080/v2/tasks

Mesos-DNS: for DNS and (presumably) service discovery

    http://master.mesos:8123/v1/version

Cluster metrics (as in http://mesos.apache.org/documentation/latest/monitoring/)

    http://master.mesos:5050/metrics/snapshot

## Cassandra
Installs cassandra on docker, so to run jobs you must use docker on the box itself

    dcos package install cassandra

To modify default settings (for instance changing cluster name and having 5 nodes instead of 3)

    dcos package install cassandra --options=cassandra-options.json

Run script or run cqlsh: ssh into master node, and do something like so

    docker run -i -t --net=host --entrypoint=/usr/bin/cqlsh spotify/cassandra -e "$script/query" cassandra-dcos-node.cassandra.dcos.mesos 9160

(for instance I replaced $script/query by $(cat hecuba-schema.cql) and that worked, although I'm hoping there's a way to pass in a file instead)

This is the framework cassandra-mesos (). Logs can be found in urls like

    /var/lib/mesos/slave/slaves/$SLAVE_ID/frameworks/$FRAMEWORK_ID/executors/$TASK_ID/runs/$RUN_ID/cassandra-mesos.log

How to interact with the database at node and cluster level: start stop repair etc: there is a REST API.  Find the relevant address and port on zookeeper (cassandra-mesos/dcos/CassandraClusterState for instance) - the REST API is also described [here](http://mesosphere.github.io/cassandra-mesos/docs/rest-api.html).

### Restoring Cassandra

A somewhat hardcoded script to retrieve the snapshots on s3 is stored in the cassandra-backup directory.
Download the snapshot, and get it onto the relevant seed node somehow.
Current method used:

* get snapshot in local directory
* scp over ssh tunnel to slave machine (like described [here](https://www.urbaninsight.com/2012/10/03/running-scp-through-ssh-tunnel) ).

copy the information to /var/lib/cassandra/data/ and run a cluster repair for good measure:

   http://<ip>:<port>/cluster/repair/start


### Cassandra GOTCHA

if we want to use cql on the database at all, we need to enable the RPC server.  This is something that is done in the cassandra.yaml configuration of cassandra itself.
At the moment cassandra is being downloaded from http://apache.cs.utah.edu/cassandra/${version.cassandra}/apache-cassandra-${version.cassandra}-bin.tar.gz
And the default for start_rpc in the cassandra.yaml is false.
How to fix manually: go to the relevant directory, cahnge the value, do a rolling restart using the REST API.

A better solution would be to take the given tar file, change it to have this done from the get-go, put it on S3 and have the correctly configured cassandra downloaded by cassandra-mesos at install.

## Spark

    dcos package install spark

Then, to run spark jobs: install dcos-spark

    git clone git@github.com:mesosphere/dcos-spark.git
    cd dcos-spark
    make env
    source env/bin/activate

Example spark job:

    dcos spark run --submit-args='-Dspark.mesos.coarse=true --driver-cores 1 --driver-memory 1024M --class org.apache.spark.examples.SparkPi http://downloads.mesosphere.com.s3.amazonaws.com/assets/spark/spark-examples_2.10-1.4.0-SNAPSHOT.jar 30'

Which translates to:

    With added env vars: {'SPARK_JAVA_OPTS': u'-Dspark.mesos.coarse=true -Dspark.mesos.executor.docker.image=mesosphere/spark:1.6.0'}
    And possibly the location of libmesos.so or libmesos.dylib in MESOS_NATIVE_PATH

    spark-submit --deploy-mode cluster --master mesos://master.mesos/service/sparkcli/ --driver-cores 1 --driver-memory 1024M --class org.apache.spark.examples.SparkPi https://downloads.mesosphere.com/spark/assets/spark-examples_2.10-1.4.0-SNAPSHOT.jar 30
    With added env vars: {'SPARK_JAVA_OPTS': u'-Dspark.mesos.coarse=true -Dspark.mesos.executor.docker.image=mesosphere/spark:1.6.0'}

The spark job jar needs to be accessible from a source that the cluster can access (in this case publicly accessible).

# Hecuba

TODO: this probably falls under "application group" as explained here https://mesosphere.github.io/marathon/docs/application-groups.html

# OpenVPN setup

bastion.yaml contains User Data for an appropriate instance. Should be added to the mesos cluster's vpn.
(ultimately something to add to the cloudformation template)
Values of bastion.yaml to be adjusted according to the explanation below.

attached a second network interface to the vpn box
https://community.openvpn.net/openvpn/wiki/BridgingAndRouting
given:

* the master subnet range of the mesos cluster
we need:
* an address range for the vpn clients (say 10.20.0.0/24)
* a second network interface on the vpn box (eth1) which is the inside-facing one
* adapt security groups accordingly (eth0 port 22 and 1194, eth1 mesos master and admin security group)

````
    # Masquerade traffic from VPN to "the world" -- done in the nat table
    iptables -t nat -I POSTROUTING -o eth0 \
          -s <openvpn client range> -j MASQUERADE
````

# <a name="dns"></a>Names of services

Two DNS servers: 

* the Amazon DNS is 10.101.0.2, which give access to the ec2 instances (based on ip-a-b-c-d.eu-central-1.compute.internal).
* Mesos-DNS: on the master IP address. gives names of the type _taskName.frameworkName.mesos_ by default

For instance:
* cassandra name: cassandra-kixi-node.cassandra.kixi.mesos
* marathon service: e.g. elasticsearch.marathon.mesos, elk.marathon.mesos

Hackery trick to find out which names are on offer: log onto the master (where mesos-dns is installed and perform):

````
    journalctl -u mesos-dns -f
````

Manually add one or two of the master boxes to your dns servers (linux: /etc/resolv.conf, mac: settings > network > advanced > DNS). This way, once you're connected to the VPN client, you have all the nice names for free (not the least master.mesos)

# Cloudformation template

The multi-master Cloudformation template creates 3 masters and 3 slaves and one "public slave" by default, with one box for the openvpn setup and one box functioning as a NAT instance between the masters and the slaves. An Elastic IP is assigned to the OpenVPN, and there are three load balancers

There is some circumstantial evidence that the json file should be minified before use.  I use the npm package json-minify for this (npm install -g json-minify).

    json-minify multi-master.cloudformation.json > mm.json

(the other way round, taking a minified file and making it readable, is easily done by using: )

    cat mm.json | python -m json.tool

The version that is saved in this repo is the expanded version, as it makes it more readable and diffable.

# Troubleshooting

if docker pull is not working properly anymore, it's worth checking for disk space (df).  Too many images can fill up the diskspace reasonably fast.
Solution

    docker rmi $(docker images -a -q)
    docker rm $(docker ps -a -q)`
    
If it doesn't release space properly, stop docker, rm -rf /var/lib/docker and restart (but this means pulling all images again).

# backup install

This is a temporary procedure and should be done automatically as soon as we can set it up.

* install awscli on slave (using pip from mesosphere) as sudo
* aws configure --profile backup
* copy cassandra-backup directory on slave (core home)
* copy backup.service and backup.timer in /etc/systemd/system and start timer (https://coreos.com/os/docs/758.1.0/scheduling-tasks-with-systemd-timers.html)

# Killing a task

destroying a marathon task is not enough to kill a task - you need to also remove it from mesos.  For instance destroying the marathon cassandra-mesos task is not enough to stop cassandra itself.  The way to stop it is to post a task to the mesos REST API directly:

    curl -d@delete.txt -X POST http://10.102.6.202:5050/master/teardown

with delete.txt containing a string which is frameworkId=xyz

If you break marathon by mistake, it's possible to restart it from master, as it's a systemd task

   sudo systemctl status dcos-marathon.service
