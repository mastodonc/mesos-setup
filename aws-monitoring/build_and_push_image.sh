#!/usr/bin/env bash

function assert_git_is_clean() {
    status=$(git status --porcelain)

    if [ -n "${status}" ]; then
    echo "Working directory is not clean. Aborting..."
    exit 1
    fi
}

assert_git_is_clean

# dockerize
TAG=git-$(git rev-parse --short=12 HEAD)
IMAGE_NAME=mastodonc/aws-cloudwatch-mon

docker build -t "${IMAGE_NAME}" .
docker tag -f "${IMAGE_NAME}" "${IMAGE_NAME}:latest" && \
docker tag -f "${IMAGE_NAME}" "${IMAGE_NAME}:${TAG}" &&
docker push "${IMAGE_NAME}"
