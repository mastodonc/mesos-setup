#!/usr/bin/env bash

# using deployment service sebastopol
TAG=git-$(git rev-parse --short=12 HEAD)
sed "s/@@TAG@@/$TAG/" aws-cloudwatch-mon.json.template > aws-cloudwatch-mon.json
echo "deploying: mastodonc/aws-cloudwatch-mon:" $TAG

# we want curl to output something we can use to indicate success/failure
STATUS=$(curl -L --post302 -s -o /dev/null -w "%{http_code}" -X POST http://$DEPLOY_IP:9501/marathon/aws-cloudwatch-mon -H "Content-Type: application/json" -H "$SEKRIT_HEADER: 123" --data-binary "@aws-cloudwatch-mon.json")
echo "HTTP code " $STATUS
if [ $STATUS == "201" ]
then exit 0
else exit 1
fi
