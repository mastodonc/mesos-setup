## Wishlist

Things to do to get a cluster that is completely what we need it to be.

* We _absolutely_ need to upgrade ALL instances to the latest version of CoreOS.  Failure to do this keeps us from using any new docker instances, which is paralyzing on CoreOS.
* IAM profiles for all boxes. THink through what we want to do: backup needs write access to s3, so does witan app, cloudwatch metrics need some other permissions, ...
* cloudwatch metrics (with aws perl docker thing) on ALL the boxes.
* cron job to clean up docker instances (well, systemd Timers https://coreos.com/os/docs/758.1.0/scheduling-tasks-with-systemd-timers.html )
* potentailly increase docker disk on public slave: 8 GB for the whole disk is proving to be too little
* incorporate backup (install awscli, put in script, systemd timers) in cloudconfig
